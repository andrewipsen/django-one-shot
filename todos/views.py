from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    list = TodoList.objects.all()

    context = {"list": list}

    return render(request, "todos/home.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)

    context = {"list": list}
    return render(request, "todos/list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/list_create.html", context)

def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "details": list,
        "form": form
    }
    return render(request, "todos/list_update.html", context)

def todo_item_create(request):
    todo_lists = TodoList.objects.all()
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
        "todo_lists": todo_lists
    }
    return render(request, "todos/list_item_create.html", context)


def todo_item_update(request, id):
    list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=list)
        if form.is_valid:
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemForm(instance=list)
    context = {
        "form": form,
        "list": list
    }
    return render(request, "todos/list_item_edit.html", context)

def todo_list_delete(request, id):
    list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/list_delete.html")
